#!/bin/sh

# Variables to be changed
MYSQL_DATABASE="grc_test"
MYSQL_USER="root"
MYSQL_PASS="root"
BACKUP_FOLDER="/Users/roshan/Desktop/backups"
TAR_BINARY="/usr/bin/tar"
MYSQL_BINARY="/Applications/MAMP/Library/bin/mysql"
LARAVEL_APPLICATION_PATH="/Applications/MAMP/htdocs/grc"

if ([ -d $BACKUP_FOLDER ] && [ -d $LARAVEL_APPLICATION_PATH ] ); then
    echo "Enter Date To Restore in this format YYYY-MM-DD :"
    read date

    if [ "$date" ] ;then 
        BACKUP_File=$BACKUP_FOLDER/$date.tar.gz

        # # Check if file exists
        if [ -f "$BACKUP_File" ]; then
            read -p "You will not be able to revert back. Continue? [Enter] → Yes, [Ctrl]+[C] → No."
            # Restore files
            $TAR_BINARY -xpzf $BACKUP_File -C $BACKUP_FOLDER/

            SQL_FILE=$BACKUP_FOLDER/db-$date.sql

            # # # Restore database
            $MYSQL_BINARY -u $MYSQL_USER --password=$MYSQL_PASS $MYSQL_DATABASE < $SQL_FILE
            # # Removing file after backup
            rm $SQL_FILE

            ## take backup of current app folder
            cp -R $LARAVEL_APPLICATION_PATH $BACKUP_FOLDER/app-backup-`date +%Y-%m-%d_%H-%M-%S`

            # # # Restore Storage Folder
            cp -R $BACKUP_FOLDER/storage-$date/ $LARAVEL_APPLICATION_PATH

            # # # Removing storage folder
            rm -rf $BACKUP_FOLDER/storage-$date

            echo Successfully resotored.

        else
            echo "Backup File for that date does not exists."
        fi
    else
        echo Please enter a date
    fi
else
    [ ! -d $BACKUP_FOLDER ] && echo "Backup directory $BACKUP_FOLDER DOES NOT exists."
    [ ! -d $LARAVEL_APPLICATION_PATH ] && echo "Application directory $LARAVEL_APPLICATION_PATH DOES NOT exists."
fi
