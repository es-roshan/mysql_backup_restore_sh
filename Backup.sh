#!/bin/sh

# Variables to be changed
MYSQL_DATABASE="database_name"
MYSQL_USER="root"
MYSQL_PASS="root"
MYSQLDUMP_BINARY="/Applications/MAMP/Library/bin/mysqldump"
TAR_BINARY="/usr/bin/tar"
BACKUP_FOLDER="/Users/roshan/Desktop/backups"

# Backup database
$MYSQLDUMP_BINARY -u $MYSQL_USER --password=$MYSQL_PASS $MYSQL_DATABASE > $BACKUP_FOLDER/db-`date +%Y-%m-%d`.sql

# compress file in backup directory
cd $BACKUP_FOLDER/ && $TAR_BINARY -cpzf `date +%Y-%m-%d`.tar.gz  db-`date +%Y-%m-%d`.sql

# remove sql file
rm $BACKUP_FOLDER/db-`date +%Y-%m-%d`.sql

# Delete old backups (older than 30 days)
# /usr/bin/find $BACKUP_FOLDER/ -mtime +30 -delete