#!/bin/sh

# Variables to be changed
MYSQL_DATABASE="database_name"
MYSQL_USER="root"
MYSQL_PASS="root"
BACKUP_FOLDER="/Users/roshan/Desktop/backups"
TAR_BINARY="/usr/bin/tar"
MYSQL_BINARY="/Applications/MAMP/Library/bin/mysql"


echo "Choose Option :"
echo "[0] Restore with latest backup"
echo "[1] Provide .sql backup file full path with filename"
echo "[Ctrl]+[C] Cancel"
read answer

if [ "$answer" != "${answer#[0]}" ] ;then 
    read -p "You will not be able to revert back. Continue? [Enter] → Yes, [Ctrl]+[C] → No."
    LAST_SQL_BACKUP=$(ls $BACKUP_FOLDER/*.tar.gz | tail -n 1)

    # Restore files
    $TAR_BINARY -xpzf $LAST_SQL_BACKUP -C $BACKUP_FOLDER/

    LAST_SQL_BACKUP=$(ls $BACKUP_FOLDER/db-*.sql | tail -n 1)

    # # Restore database
    $MYSQL_BINARY -u $MYSQL_USER --password=$MYSQL_PASS $MYSQL_DATABASE < $LAST_SQL_BACKUP
    # # Removing file after backup
    # rm $LAST_SQL_BACKUP

    echo Successfully resotored.
elif [ "$answer" != "${answer#[1]}" ] ;then
    echo "Enter file with full path (eg. /user/grc/backup.sql) :"
    read filewithpath

    # # Check if file exist
    if [ -e $filewithpath ]
    then
        read -p "You will not be able to revert back. Continue? [Enter] → Yes, [Ctrl]+[C] → No."
        $MYSQL_BINARY -u $MYSQL_USER --password=$MYSQL_PASS $MYSQL_DATABASE < $filewithpath
        echo Successfully resotored.
    else
        echo "Could not locate the file."
    fi
    
else
    echo Please choose a valid option
fi