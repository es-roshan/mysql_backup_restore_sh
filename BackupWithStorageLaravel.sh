#!/bin/sh

# Variables to be changed
MYSQL_DATABASE="grc"
MYSQL_USER="root"
MYSQL_PASS="root"
MYSQLDUMP_BINARY="/Applications/MAMP/Library/bin/mysqldump"
TAR_BINARY="/usr/bin/tar"
BACKUP_FOLDER="/Users/roshan/Desktop/backups"
LARAVEL_APPLICATION_PATH="/Applications/MAMP/htdocs/grc"

if ([ -d $BACKUP_FOLDER ] && [ -d $LARAVEL_APPLICATION_PATH ] ); then
    # Backup database
    $MYSQLDUMP_BINARY -u $MYSQL_USER --password=$MYSQL_PASS $MYSQL_DATABASE > $BACKUP_FOLDER/db-`date +%Y-%m-%d`.sql

    STORAGE_PATH=$LARAVEL_APPLICATION_PATH
    STORAGE_BACKUP_FOLDER=storage-`date +%Y-%m-%d`

    # copy storage to backup folder
    cd $BACKUP_FOLDER  && cp -R $STORAGE_PATH $BACKUP_FOLDER/$STORAGE_BACKUP_FOLDER

    # compress file in backup directory
    cd $BACKUP_FOLDER/ && $TAR_BINARY -cpzf `date +%Y-%m-%d`.tar.gz  db-`date +%Y-%m-%d`.sql $STORAGE_BACKUP_FOLDER

    # remove sql and backup file
    rm $BACKUP_FOLDER/db-`date +%Y-%m-%d`.sql
    rm -rf $BACKUP_FOLDER/$STORAGE_BACKUP_FOLDER

    # Delete old backups (older than 30 days)
    # /usr/bin/find $BACKUP_FOLDER/ -mtime +30 -delete
else
    [ ! -d $BACKUP_FOLDER ] && echo "Directory $BACKUP_FOLDER DOES NOT exists."
    [ ! -d $LARAVEL_APPLICATION_PATH ] && echo "Directory $LARAVEL_APPLICATION_PATH DOES NOT exists."
fi