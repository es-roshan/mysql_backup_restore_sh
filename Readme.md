## BACKUP Database and App 
##### It dump the database to sql and backup whole app folder and zip it to provided backup folder with filename as date.

- First fill all the required variable needed in BackupWithStorageLaravel.sh file
- MYSQL_DATABASE="database_name" (database name to keep backup)
- MYSQL_USER="root"
- MYSQL_PASS="root"
- MYSQLDUMP_BINARY="/Applications/MAMP/Library/bin/mysqldump" ( give full path of your mysqldump executable file )
- TAR_BINARY="/usr/bin/tar" ( give full path of your tar executable file )
- BACKUP_FOLDER="/Users/roshan/Desktop/backups" ( choose a folder to keep backup * it should be a directory)
- LARAVEL_APPLICATION_PATH="/app/base/path" ( choose your laravel main project folder * it should be a directory)
- chmod u+x BackupWithStorageLaravel.sh (Make file executable)
- ./BackupWithStorageLaravel.sh (run the file)


## Restore Database and App 
##### It restore the given database and project storage in given input date.
- First fill all the required variable needed in RestoreWithStorageLaravel.sh file
- MYSQL_DATABASE="database_name" (database name to restore)
- MYSQL_USER="root"
- MYSQL_PASS="root"
- MYSQL_BINARY="/Applications/MAMP/Library/bin/mysql" ( give full path of your mysql executable file )
- TAR_BINARY="/usr/bin/tar" ( give full path of your tar executable file )
- BACKUP_FOLDER="/Users/roshan/Desktop/backups" ( Same folder where you backup files with above BackupWithStorageLaravel.sh file)
- LARAVEL_APPLICATION_PATH="/app/base/path" ( choose your laravel main project folder * it should be a directory)
- chmod u+x RestoreWithStorageLaravel.sh (Make file executable)
- ./RestoreWithStorageLaravel.sh (run the file)
- Enter date in format YYYY-MM-DD, eg. 2021-11-06 ( It check if that date backup folder exists or not and restore it)


## Creating cronjob for backup database and storage everyday at 1am (linux)
- In terminal type ```crontab -e```
- add this line ```0 1 * * * /path/to/Backup.sh/file/BackupWithStorageLaravel.sh```
- If it is opened with Vim, to save after adding line , Press ```ESC``` button , Press ```Shift + :``` and Type ```wq``` and Press ```Enter```.



## BACKUP ONLY Database
##### It dump the database to sql and zip it to provided backup folder with filename as date.

- First fill all the required variable needed in Backup.sh file
- MYSQL_DATABASE="database_name" (database name to keep backup)
- MYSQL_USER="root"
- MYSQL_PASS="root"
- MYSQLDUMP_BINARY="/Applications/MAMP/Library/bin/mysqldump" ( give full path of your mysqldump executable file )
- TAR_BINARY="/usr/bin/tar" ( give full path of your tar executable file )
- BACKUP_FOLDER="/Users/roshan/Desktop/backups" ( choose a folder to keep backup * it should be a directory)
- chmod u+x Backup.sh (Make file executable)
- ./Backup.sh (run the file)

## Restore ONLY Database
##### It restore the given database. You can choose two options, to restore database with the latest previously created backup file or you can provide sql file.
- First fill all the required variable needed in Restore.sh file
- MYSQL_DATABASE="database_name" (database name to restore)
- MYSQL_USER="root"
- MYSQL_PASS="root"
- MYSQL_BINARY="/Applications/MAMP/Library/bin/mysql" ( give full path of your mysql executable file )
- TAR_BINARY="/usr/bin/tar" ( give full path of your tar executable file )
- BACKUP_FOLDER="/Users/roshan/Desktop/backups" ( Same folder where you backup files with above Backup.sh file)
- chmod u+x Restore.sh (Make file executable)
- ./Restore.sh (run the file)
- Proceed according to the output.


## Creating cronjob for backup database everyday at 1am (linux)
- In terminal type ```crontab -e```
- add this line ```0 1 * * * /path/to/Backup.sh/file/Backup.sh```
- If it is opened with Vim, to save after adding line , Press ```ESC``` button , Press ```Shift + :``` and Type ```wq``` and Press ```Enter```.